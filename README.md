# Stigdata

Logs discord messages to supabase.

## Philosophy References

"Stig" is for Stigliani

- [Stigliani and Ravasi "ORGANIZING THOUGHTS AND CONNECTING BRAINS:
MATERIAL PRACTICES ... SENSEMAKING"](https://mycourses.aalto.fi/pluginfile.php/1348206/mod_resource/content/2/04%20Stigliani%2C%20Ravasi%20-%202012%20-%20Organizing%20Thoughts%20and%20Connecting%20Brains%20Material%20Practices%20and%20the%20Transition%20From%20Individual%20To%20Group-Leve.pdf)
- [Bonnitta Roy "Sensmaking Up-Hierarchies: Information Flows for Emmergent Complexity"](https://medium.com/agile-sensemaking/sensemaking-up-hierarchies-a4eea852c139)
- [Jonny Saunders Dissertation, 2022 "Swarmpunk: Rough Consensus and Running Code in Brains, Machines, and Society"](https://jon-e.net/dissertation/)


## Set up an environment

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```


## [Supabase API tutorial](README-supabase.md)

Supabase - provides the REST and websockets API for the Postgres DB.
Redis - cache messages (API calls & responses) in RAM.
Uvicorn - web server to handle websocket connection requests
Fastapi - web framework connected to Uvicorn.
Deta Cloud - to deploy


### PyCord References

- [Documentation](https://docs.pycord.dev/en/master/index.html)
- [Learn how to create Discord bots with Pycord](https://guide.pycord.dev)_
- [PyCord Discord Server](https://pycord.dev/discord)_
- [PyCord Developers Server](https://discord.gg/discord-developers)_
- [Unofficial Discord API Server](https://discord.gg/discord-api)