"""

1. visit the discord developer dashboard and generate a URL for a bot with admin privileges (the permissions bit mask integer in the URL should be long).

2. start the discord bot service

>>> python hello_message_client
We have logged in as daotest#7533

3. Visit the discord server you generated the URL for and you should see your bot among the list of onine users.
4. In the online users list on discord right click the bot and invite them to the server.
5. In discord say "hello what's up?", lowercase hello is required, (on any public channel)?

You should see something like this logged in the terminal where the bot is running.
This is the message the bot posted on the server (because that's the only print statement in the on_message fun)
This is **NOT** the original message from the user:

Received message from bot (daotest#7533):
    Hello! You just said: hello what's up?
message_obj:
<Message
    id=1009925779129389138
    channel=<TextChannel
        id=992807246788567134
        name='general'
        position=4
        nsfw=False
        category_id=992807246788567133
        news=False>
    type=<MessageType.default: 0>
    author=<Member
        id=1007105836092497950
        name='daotest'
        discriminator='7533'
        bot=True
        nick=None
        guild=<Guild
            id=992807246113276064
            name='qary'
            shard_id=0
            chunked=False
            member_count=7
            >
        >
    flags=<MessageFlags value=0>
>


"""
import datetime
import discord
import json
import os  # default module
from supabase import create_client
import config
# import json


SUPA = create_client(
    config.SUPABASE_URL, config.SUPABASE_KEY)

from dotenv import load_dotenv

intents = discord.Intents.default()
intents.message_content = True

load_dotenv()  # load all the variables from the env file

AUTHOR_ATTRS = 'name discriminator bot display_name'.split()
REACTION_ATTRS = 'emoji count me'.split()

client = discord.Client(intents=intents)


@client.event
async def on_ready():
    print(f'Bot named {client.user} listening ')


def get_coroutine(obj, attr_path='channel.send'):
    coro = obj
    for attr in attr_path.split('.'):
        coro = getattr(coro, attr)
    return coro


async def await_action(message, action='channel.send', *args, **kwargs):
    await get_coroutine(message, action)(*args, **kwargs)


@client.event
async def on_message(message):
    print()
    print(f'Received message from {message.author}:')
    # print(f'    vars(message):\n        {vars(message)}')
    # print(f'    dir(message):\n        {dir(message)}')
    print()

    # don't accidentally reply to your own messages (infinite recursion)!!!
    if message.author == client.user:
        return

    action_args = [
        ('add_reaction', ['✍️'], {}),
        ('reply', ['noted'], {}),
        # TODO allow format string templates with `def render_fstring(fstring, message):`
        # ('channel.send', ['noted'], {}),
    ]
    # # emoji = discord.Emoji(guild=, state=, data=)

    # # https://docs.pycord.dev/en/master/api.html#discord.Message.add_reaction
    # print(f'Reacting: {emoji_name}')
    # await message.add_reaction(emoji)
    # text = f"I noted this {message.author}"
    # print(f'Sending: {text}')
    # await get_coroutine(message, 'channel.send')(text)

    # text = f"Noted!"
    # print(f'Replying: {text}')

    if message.content:
        for action, args, kwargs in action_args:
            args = args if isinstance(args, list) else [args]
            kwargs = {} if kwargs is None else dict(kwargs)
            print(f'{action}(*{args}, **{kwargs})')
            await get_coroutine(message, action)(*args, **kwargs)

        # # emoji = discord.Emoji(guild=, state=, data=)

        # # https://docs.pycord.dev/en/master/api.html#discord.Message.add_reaction
        # print(f'Reacting: {emoji_name}')
        # await message.add_reaction(emoji)
        # text = f"I noted this {message.author}"
        # print(f'Sending: {text}')
        # await get_coroutine(message, 'channel.send')(text)

        # text = f"Noted!"
        # print(f'Replying: {text}')


@client.event
async def on_reaction_add(reaction, member):
    print(f'\non_reaction_add\nreaction={type(reaction)}({reaction})\nmember={type(member)}({member})')
    # print(f'Received reaction from {reaction.author}:')
    # print(f'    {reaction}:')
    # users = await reaction.users().flatten()
    # print(f'Total unique user reactions of this type?: {len(users)}')


client.run(os.getenv('BOT_TOKEN'))
