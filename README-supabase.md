# Stigdata

Logs discord messages to supabase.

## References

"Stig" is for Stigliani

- [Stigliani and Ravasi "ORGANIZING THOUGHTS AND CONNECTING BRAINS:
MATERIAL PRACTICES ... SENSEMAKING"](https://mycourses.aalto.fi/pluginfile.php/1348206/mod_resource/content/2/04%20Stigliani%2C%20Ravasi%20-%202012%20-%20Organizing%20Thoughts%20and%20Connecting%20Brains%20Material%20Practices%20and%20the%20Transition%20From%20Individual%20To%20Group-Leve.pdf)
- [Bonnitta Roy "Sensmaking Up-Hierarchies: Information Flows for Emmergent Complexity"](https://medium.com/agile-sensemaking/sensemaking-up-hierarchies-a4eea852c139)
- [Jonny Saunders Dissertation, 2022 "Swarmpunk: Rough Consensus and Running Code in Brains, Machines, and Society"](https://jon-e.net/dissertation/)


### Supabase API tutorial

Supabase - provides the REST and websockets API for the Postgres DB.
Redis - cache messages (API calls & responses) in RAM.
Uvicorn - web server to handle websocket connection requests
Fastapi - web framework connected to Uvicorn.
Deta Cloud - to deploy


### Setting up an environment

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

After setting up Supabase account, you need to create a `.env` file with the following:

```bash
SUPABASE_URL=<Supabase Project URL>
SUPABASE_KEY=<Supabase Anon Project Key>
REDIS_URL=redis://127.0.0.1:6379
```

### Running locally

Run redis and Uvicorn in the background in two tmux or terminal screens or run them in the background.

```bash
redis-server &
```


```bash
uvicorn main:app --reload &
```


On success of the commad you should see;

```zsh
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Started reloader process [13385] using watchgod
INFO:     Started server process [13387]
2022-02-11 19:32:12,509:INFO - Started server process [13387]
INFO:     Waiting for application startup.
2022-02-11 19:32:12,509:INFO - Waiting for application startup.
2022-02-11 19:32:12,510:INFO -  02/11/2022 07:32:12 PM | CONNECT_BEGIN: Attempting to connect to Redis server...
2022-02-11 19:32:12,511:INFO -  02/11/2022 07:32:12 PM | CONNECT_SUCCESS: Redis client is connected to server.
INFO:     Application startup complete.
2022-02-11 19:32:12,511:INFO - Application startup complete.
```

### 🎾 Endpoints

Introduction to your application.
```bash
http "http://127.0.0.1:8000/"

HTTP/1.1 200 OK
content-length: 102
content-type: application/json
date: Wed, 16 Feb 2022 22:01:14 GMT
server: uvicorn

{
    "👋 Hello": "Please refer to the readme documentation for more or visit http://localhost:8000/docs"
}
```

Working with your redis cache, the following call will pull data
from your supabase database, and cache it.

The x-fastapi-cache header field indicates that this response was found in the Redis cache (a.k.a. a Hit).

The only other possible value for this field is Miss. The expires field and max-age value in the cache-control field indicate that this response will be considered fresh for 604321 seconds(1 week). This is expected since it was specified in the @cache decorator.

The etag field is an identifier that is created by converting the response data to a string and applying a hash function. If a request containing the if-none-match header is received, any etag value(s) included in the request will be used to determine if the data requested is the same as the data stored in the cache. If they are the same, a 304 NOT MODIFIED response will be sent. If they are not the same, the cached data will be sent with a 200 OK response.

```bash
# Command
http "http://127.0.0.1:8000/cachedResults"

# Response Headers
HTTP/1.1 200 OK
cache-control: max-age=604321
content-length: 894
content-type: application/json
date: Wed, 16 Feb 2022 21:53:56 GMT
etag: W/-9174636245072902018
expires: Wed, 23 Feb 2022 21:45:57 GMT
server: uvicorn
x-supafast-cache: Hit
```


### Docs

- [Installing Redis](https://redis.io/topics/quickstart)
- [Setting up Supabase](https://supabase.com/docs/reference)
- [Getting started with FastApi](https://fastapi.tiangolo.com/tutorial/)
- [Tutorial Author](https://github.com/cloudguruab)
