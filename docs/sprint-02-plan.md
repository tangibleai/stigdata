# sprint-02-plan.md

Aug 11 - Aug 18

## Sprint 02

- H1: redis + uvicorn + pycord + ascync environment
- H1: hello discord bot on qary discord server
- H1: supabase schema for holding messages
- H1: push one discord message to supabase
- H1: connect supabase settings.DATABASES = {} to stigsite/app
- H1: send links and access to all repos in daostack
- H1: access to discord qary server
- H1: access to env file on bitwarden and discord credentials
- H1: invite greg to daostack daogarden server
- H4: deploy stigdata to stigdata.onrender.com


- G1: sign up for discord and play with it and subscribe/join servers that interest you
- G4: write down and organize notes about what all these files are and what they are for
- G4: go through the discord python bot examples that you can find
- G2: modify the example discord bot from tutorial to retrieve many messages from discord history
- G1: create fastapi demo site from tutorial
- G1: use huggingface python webapp package to create huggingface spaces site for a text box input/output
- G4: deploy empty django app stigsite to render stigsite.onrender.com or similar

## Sprint 01

- got a discord bot (stigdata) working with qary server `/hello` pycord+uvicorn+redis