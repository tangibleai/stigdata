""" Bot() instance that registers `/hello` slash command (1 hr to register!)

Let’s look at the differences compared to the previous example, step-by-step:

    The first line remains unchanged.

    Next, we create an instance of Bot. This is different from Client, as it supports slash command creation and other features, while inheriting all the features of Client.

    We then use the Bot.slash_command() decorator to register a new slash command. The guild_ids attribute contains a list of guilds where this command will be active. If you omit it, the command will be globally available, and may take up to an hour to register.

    Afterwards, we trigger a response to the slash command in the form of a text reply. Please note that all slash commands must have some form of response, otherwise they will fail.

    Finally, we, once again, run the bot with our login token.

Congratulations! Now you have created your first slash command!
"""
import discord
import os  # default module
from dotenv import load_dotenv

load_dotenv()  # load all the variables from the env file
bot = discord.Bot()  # debug_guilds=[881207955029110855])


@bot.event
async def on_ready():
    print(f"{bot.user} is ready and online!")


@bot.slash_command(name="hello", description="Say hello to the bot")
async def hello(context):
    print(f"Recieved /hello command with\n       context: {context}\n  vars(context): {vars(context)}")
    await context.respond("Hi! This is {bot.user}. I only know the /hello command... for now.")


@bot.slash_command(name="hi", description="Say hi to the bot")
async def hi(context):
    print(f"Recieved /hello command with\n       context: {context}\n  vars(context): {vars(context)}")
    await context.respond("Hi! This is {bot.user}. I only know the /hello command... for now.")


@bot.event
async def on_message(message):
    if message.author == bot.user:
        print(f'Received message from bot ({bot.user}): {message.content}\nmessage_obj: {message}')
        return

    if message.content.startswith('$hello'):
        await message.channel.send(f'Hello! You just said: {message.content}')


bot.run(os.getenv('BOT_TOKEN'))  # run the bot with the token
