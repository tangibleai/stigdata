import os

from dotenv import load_dotenv

load_dotenv()


SUPABASE_URL = os.getenv("SUPABASE_URL")

SUPABASE_KEY = os.getenv("SUPABASE_KEY")
print(f"SUPABASE_KEY ...{SUPABASE_KEY[3:6]}...")

REDIS_URL = os.getenv("REDIS_URL")
print(f"REDIS_URL ...{REDIS_URL}...")
