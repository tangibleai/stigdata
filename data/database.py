# variables for database and url configuration
from config import Config

from supabase import Client, create_client


class SupabaseDB:
    """
    class instance for database connection to supabase

    :str: url: configuration for database url for data inside supafast project
    :str: key: configuration for database secret key for authentication
    :object: supabase: Supabase instance for connection to database environment
    """

    supabase_url: str = Config.SUPABASE_URL
    supabase_key: str = Config.SUPABASE_KEY
    # print(f"supabase_key: ...{supabase_key[3:6]}...")
    supabase: Client = create_client(supabase_url=supabase_url, supabase_key=supabase_key)
