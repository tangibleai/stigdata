# Pycord Tutorial

The source code is at: https://github.com/Pycord-Development/pycord

## Environment

```bash
python3 -m venv env
source env/bin/activate
python --version
```

**Python 3.8 or higher is required!!!!**

For a basic discord API (without voice):

```bash
python -m pip install -U py-cord
```

For voice support:

```bash
python -m pip install -U "py-cord[voice]"
```

If you want to speed things up:

```bash
python -m pip install -U "py-cord[speed]"
```

### Optional dependencies

* `PyNaCl`
* `aiodns`
* `brotlipy`
* `cchardet` (for `aiohttp` speedup)
* `orjson` (for json speedup)

### OS dependencies

* apt install libffi-dev (or `libffi-devel` on some systems)
* apt install python-dev (or `python3.10-dev` for Python 3.10)


### Examples

Slash command:

```python
import discord

bot = discord.Bot()

@bot.slash_command()
async def hello(ctx, name: str = None):
    name = name or ctx.author.name
    await ctx.respond(f"Hello {name}!")

@bot.user_command(name="Say Hello")
async def hi(ctx, user):
    await ctx.respond(f"{ctx.author.mention} says hello to {user.name}!")

bot.run("token")
```

Traditional command:

```python
import discord
from discord.ext import commands

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix=">", intents=intents)

@bot.command()
async def ping(ctx):
    await ctx.send("pong")

bot.run("token")
```

### PyCord References

- [Documentation](https://docs.pycord.dev/en/master/index.html)
- [Learn how to create Discord bots with Pycord](https://guide.pycord.dev)_
- [PyCord Discord Server](https://pycord.dev/discord)_
- [PyCord Developers Server](https://discord.gg/discord-developers)_
- [Unofficial Discord API Server](https://discord.gg/discord-api)
